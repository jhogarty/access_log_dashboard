# README

This is a simple weekend pet project I worked on to assist me at the day job.  The purpose of this Ruby application is to provide an easy way to ingest our APIs access log file and then do some basic reporting on that data.  This project uses SQLite3 as the database and you can use either the in-memory option or file based database option.  See comments in the log_reader.rb file.

To spice things up, this project will present that data as a terminal based dashboard.  Preview the example.png file to see an example of what the application can do.

The application expects your access log file to be formatted as follows:

  ```shell
  # file: access.log

  [2020-04-10 13:33:35 +0000] INFO -- 200 -- db=0.0 total=0.0593 view=0.0593 -- GET /demo-api/v1/status/version host=localhost
  [2020-04-10 13:33:36 +0000] INFO -- 200 -- db=0.0 total=0.0593 view=0.0593 -- GET /demo-api/v1/status/db-status host=localhost
  ```

You will then need to create an 'import' file as follows:

  ```shell
  cat access.log | cut -d' ' -f1,2,6,9,12,13 | sed 's/\[//g' | sed 's/total=//g' > import_file.csv
  ```

**IMPORTANT** out of the box the app is configured to use SQLite with a file.  If you do not want to keep adding duplicated data, see the NOTE around line 43 of log_reader.rb and comment out the IO.foreach block.

In the future, if time/interest permits I'd like to add command line options to this app to define if using SQLite as an in-memory database or file based database.  If choosing file based, there would be an option for bypassing/using the IO.foreach block.

