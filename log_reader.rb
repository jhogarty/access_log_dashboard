#!/usr/bin/env ruby

# frozen_string_literal: true

require 'sequel'
require 'csv'
require 'tty-pie'
require 'tty-box'
require 'tty-table'
require 'pry'

# cat access.log | cut -d' ' -f1,2,6,9,12,13 | sed 's/\[//g' | sed 's/total=//g' > import_file.csv
# Data Source Format (import_file.csv):
#   2020-04-10 13:33:36 200 0.0593 GET /cloudmeta-api/v1/status/db-status
IMPORT_FILE   = '/path/to/your/projects/access_log_dashboard/new_import_file.csv'
DATABASE_FILE = '/path/to/your/projects/access_log_dashboard/access_log.db'  # NOTE: file tends to get large, do not check into source control

# DB = Sequel.sqlite()                      # NOTE: in memory database, data does not persist
DB = Sequel.sqlite(database: DATABASE_FILE) # NOTE: file based database, data persists

# DB populated, so let's not waste time :)
DB.create_table? :access_logs do
  primary_key :id
  DateTime    :timestamp, null: false
  Integer     :http_code, null: false
  Float       :total, null: false
  String      :http_method, null: false
  String      :end_point, null: false
end

class AccessLog < Sequel::Model(:access_logs)
end

def inputs_valid?(date, time, status, resp_time, method, end_point)
  date &&
    time &&
    status &&
    resp_time &&
    method &&
    end_point
end

# NOTE: Comment out once data is imported, if using a database file vs. in-memory
IO.foreach(IMPORT_FILE) do |row|
  log = DB[:access_logs]
  date, time, status, resp_time, method, end_point = row.chomp.split
  next unless inputs_valid?(date, time, status, resp_time, method, end_point)
  log.insert(timestamp: "#{date} #{time}", http_code: status.to_i, total: resp_time.to_f, http_method: method, end_point: end_point)
end

END_POINTS = [
  { label: :login, http_method: 'POST', end_point: '%/login' },
  { label: :hosts, http_method: 'GET', end_point: '%/hosts' },
  { label: :host_attrs, http_method: 'GET', end_point: '%/hosts/%/attrs' },
  { label: :host_attrs_attr, http_method: 'GET', end_point: '%/hosts/%/attrs/%' },
  { label: :pods, http_method: 'GET', end_point: '%/pods' },
  { label: :pod_attrs, http_method: 'GET', end_point: '%/pods/%/attrs' },
  { label: :pod_attrs_attr, http_method: 'GET', end_point: '%/pods/%/attrs/%' },
  { label: :pod_attrs_arch, http_method: 'GET', end_point: '%/pods/%/attrs/architecture' },
  { label: :server_save, http_method: 'POST', end_point: '%/server/save' },
  { label: :exa_clus_cells, http_method: 'POST', end_point: '%/exadata_clusters/%/cells' },
  { label: :exa_clus_nodes, http_method: 'POST', end_point: '%/exadata_clusters/%/nodes' },
  { label: :pod_save, http_method: 'POST', end_point: '%/pod/save' },
]


# ---------------------------------------------------------
# DATABASE PREPARED STATMENTS
# ---------------------------------------------------------
DB["Select COUNT(*)   as count from access_logs where http_method = ? and end_point like ?", :$http_method, :$end_point].prepare(:select, :end_point_count)
DB["Select MIN(total) as total_min from access_logs where http_method = ? and end_point like ?", :$http_method, :$end_point].prepare(:select, :end_point_min)
DB["Select MAX(total) as total_max from access_logs where http_method = ? and end_point like ?", :$http_method, :$end_point].prepare(:select, :end_point_max)
DB["Select AVG(total) as total_avg from access_logs where http_method = ? and end_point like ?", :$http_method, :$end_point].prepare(:select, :end_point_avg)
# ---------------------------------------------------------


# ---------------------------------------------------------
# QUERY DATA
# ---------------------------------------------------------
total_count  = DB[:access_logs].count
post_count   = DB[:access_logs].where(http_method: 'POST').count
get_count    = AccessLog.where(http_method: 'GET').count
put_count    = AccessLog.where(http_method: 'PUT').count
patch_count  = AccessLog.where(http_method: 'PATCH').count
delete_count = AccessLog.where(http_method: 'DELETE').count
host_counts  = DB.call(:end_point_count, http_method: 'GET', end_point: '%/hosts')
pod_counts   = DB.call(:end_point_count, http_method: 'GET', end_point: '%/pods')
login_posts  = DB.call(:end_point_count, http_method: 'POST', end_point: '%/login')
# ---------------------------------------------------------


# ---------------------------------------------------------
# QUERIES AND DATA COLLECTION
# ---------------------------------------------------------
end_point_counts = []
END_POINTS.each do |item|
  count = DB.call(:end_point_count, http_method: item[:http_method], end_point: item[:end_point]).first
  end_point_counts << item.merge(count)
end

end_point_min = []
END_POINTS.each do |item|
  min = DB.call(:end_point_min, http_method: item[:http_method], end_point: item[:end_point]).first
  min.transform_values! { |value| value.round(4) } unless min[:total_min].nil?
  end_point_min << item.merge(min)
end

end_point_max = []
END_POINTS.each do |item|
  max = DB.call(:end_point_max, http_method: item[:http_method], end_point: item[:end_point]).first
  max.transform_values! { |value| value.round(4) } unless max[:total_max].nil?
  end_point_max << item.merge(max)
end

end_point_avg = []
END_POINTS.each do |item|
  avg = DB.call(:end_point_avg, http_method: item[:http_method], end_point: item[:end_point]).first
  avg.transform_values! { |value| value.round(4) } unless avg[:total_avg].nil?
  end_point_avg << item.merge(avg)
end
# ---------------------------------------------------------


# ---------------------------------------------------------
# DEFINE PIE CHARTS
# ---------------------------------------------------------
http_method_counts = [
  { name: 'POST', value: post_count, color: :bright_yellow, fill: '*' },
  { name: 'GET', value: get_count, color: :bright_magenta, fill: 'x' },
  { name: 'PUT', value: put_count, color: :bright_cyan, fill: '+' },
  { name: 'PATCH', value: patch_count, color: :bright_white, fill: '@' },
  { name: 'DELETE', value: delete_count, color: :bright_red, fill: '-' }
]
http_methods_pie_chart = TTY::Pie.new(data: http_method_counts, top: 4, radius: 5)

host_and_pod = [
  { name: 'HOSTS', value: host_counts.first[:count], color: :bright_cyan, fill: '+' },
  { name: 'PODS', value: pod_counts.first[:count], color: :bright_yellow, fill: '-' }
]
host_pod_pie_chart = TTY::Pie.new(data: host_and_pod, top: 4, left: 40, radius: 5)

posts_login_other = [
  { name: 'Login', value: login_posts.first[:count], color: :bright_white, fill: '+' },
  { name: 'Other', value: post_count, color: :bright_red, fill: '-' }
]
login_other_pie_chart = TTY::Pie.new(data: posts_login_other, top: 4, left: 80, radius: 5)
# ---------------------------------------------------------


# ---------------------------------------------------------
# TABLES (to go in boxes)
# ---------------------------------------------------------
table_main = TTY::Table.new ['HTTP METHOD','TOTAL COUNT'],[
  ['Post', post_count],
  ['Get', get_count],
  ['Put', put_count],
  ['Patch', patch_count],
  ['Delete', delete_count]
]

table_count_rows = []
end_point_counts.each do |key, value|
  table_count_rows << [key[:http_method], key[:end_point], key[:count]]
end
table_counts = TTY::Table.new ['HTTP METHOD','END POINT', 'TOTAL COUNT'], table_count_rows

table_min_rows = []
end_point_min.each do |key, value|
  table_min_rows << [key[:http_method], key[:end_point], key[:total_min]]
end
table_mins = TTY::Table.new ['HTTP METHOD','END POINT', 'MINIMUM'], table_min_rows

table_max_rows = []
end_point_max.each do |key, value|
  table_max_rows << [key[:http_method], key[:end_point], key[:total_max]]
end
table_maxs = TTY::Table.new ['HTTP METHOD','END POINT', 'MAXIMUM'], table_max_rows

table_avg_rows = []
end_point_avg.each do |key, value|
  table_avg_rows << [key[:http_method], key[:end_point], key[:total_avg]]
end
table_avgs = TTY::Table.new ['HTTP METHOD','END POINT', 'AVERAGE'], table_avg_rows
# ---------------------------------------------------------


# ---------------------------------------------------------
# BOXES (to hold tables)
# ---------------------------------------------------------
box_main = TTY::Box.frame(top: 16, left: 4, width: 30, height: 12) do
  table_main.render(:ascii)
end

box_counts = TTY::Box.frame(top: 28, left: 4, width: 56, height: 18) do
  table_counts.render(:ascii)
end

box_mins = TTY::Box.frame(top: 46, left: 4, width: 56, height: 18) do
  table_mins.render(:ascii)
end

box_maxs = TTY::Box.frame(top: 46, left: 64, width: 56, height: 18) do
  table_maxs.render(:ascii)
end

box_avgs = TTY::Box.frame(top: 28, left: 64, width: 56, height: 18) do
  table_avgs.render(:ascii)
end
# ---------------------------------------------------------


# ---------------------------------------------------------
# DISPLAY RESULTS
# ---------------------------------------------------------
# binding.pry  # AccessLog.db_schema (show table schema)
puts "============================================================================================================================="
puts "| SUMMARY INFO --> ACCESS LOG DASHBOARD                                                                                     |"
puts "============================================================================================================================="
print http_methods_pie_chart
puts "\n"
print host_pod_pie_chart
puts "\n"
print login_other_pie_chart
puts "\n\n"
print box_main
print box_counts
print box_avgs
print box_mins
print box_maxs
print "\n\n"
puts "Total Count: #{total_count}"
# ---------------------------------------------------------

